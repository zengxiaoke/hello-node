const Hapi = require('@hapi/hapi');

init();

async function init() {
  // create server
  const server = Hapi.server({
    host: 'localhost',
    port: '3000',
  });

  // auth
  await server.register(
    // cookie auth
    require('@hapi/cookie')
  );
  server.auth.strategy('cookie-one', 'cookie', {
    cookie: {
      name: 'sid',
      // at least 32 chars
      password: '!wsYhFA*C2U6nz=Bu^%A@^F#SF3&kSR6',
      // false for local dev
      // isSecure: true,
    },
    redirectTo: '/login',
    // the validation logic
    validate: async (request, session) => {
      if (!session?.id) {
        return { isValid: false };
      }

      return { isValid: true, credentials: session };
    },
  });
  // default auth
  server.auth.default('cookie-one');

  // login page
  server.route({
    method: 'get',
    path: '/login',
    options: {
      auth: false,
    },
    handler(request, h) {
      return 'input something to login';
    },
  });

  // login post
  server.route({
    method: 'post',
    path: '/login',
    options: {
      auth: false,
    },
    handler(request, h) {
      // log the user in
      request.cookieAuth.set({ id: 'arche' });
      return 'logged in, good to go.';
    },
  });

  // logout
  server.route({
    method: 'get',
    path: '/logout',
    handler(request, h) {
      // log the user out
      request.cookieAuth.clear();
      return 'logged out...';
    },
  });

  // home page
  server.route({
    method: 'get',
    path: '/',
    options: {
      auth: false,
    },
    handler(req, h) {
      return 'home page';
    },
  });

  // biz page
  server.route({
    method: 'get',
    path: '/biz',
    handler(req, h) {
      const session = req.auth.credentials;
      return 'do your biz...' + JSON.stringify(session);
    },
  });

  // start server
  await server.start();
  console.log('started server at localhost:3000');
}
