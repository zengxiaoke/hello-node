// @ts-nocheck

import express = require('express');
import cookieSession = require('cookie-session');

const app = express();

learnCookieSession();

app.listen(9527, () => {
  console.log('app started on port 9527');
});

function learnCookieSession() {
  app.use(
    cookieSession({
      name: 'b2b',
      keys: ['sign-key', 'verify-key'],
    })
  );

  app.route('/').get((req, res) => {
    const who = req.session.who;
    res.send(who ? `Hi, ${who.name} ${who.role}` : 'maybe login first!!!');
  });

  app.get('/login', (req, res) => {
    const who = { name: 'bob', role: 'boss' };
    req.session.who = who;
    res.send('welcome back: ' + who.name);
  });
}
