import express = require('express');
import path = require('path');

const app = express();

// serve static assets
app.use(express.static(path.resolve(__dirname, '../public')));

learnRouter();
// learnMiddleware();
// learnOverriding();
// learnErrorHandling();
// learnDebug();

function learnRouter() {
  // serve route
  // path can be string pattern or regex
  // ? + * ()
  // $: /data/([\$])book
  /**
Route path: /users/:userId/books/:bookId
Request URL: http://localhost:3000/users/34/books/8989
req.params: { "userId": "34", "bookId": "8989" }
 */
  const routePath = '/get-:who(\\d+)';
  app.get(routePath, (req, res, next) => {
    res.json({
      name: req.params.who,
      age: 20,
    });

    // next('route')

    /** end the res */
    // res.download()
    // res.end()
    // res.json()
    // res.jsonp()
    // res.redirect()
    // res.render()
    // res.send()
    // res.sendFile()
    // res.sendStatus()
  });

  /** chainable router */
  // app.route('/here').get().post().put

  /** router module */
  // const router = express.Router();
  // router.use();
  // router.get('/');
  // app.use('/sub-router', router);
}

function learnMiddleware() {
  app.use(async (req, res, next) => {
    console.log('before cookie...');
    await req.cookies;
    console.log('after cookie...');
    // pass in anything except for 'route' and 'router' will cause error
    next();
  });

  app.use((req, res, next) => {
    console.log('getting...', req.url);
    // not call next() will handing
    next();
  });

  // order is important
  app.get('/', (req, res) => {
    res.send('hello world');
  });

  // make configurable by middleware factory

  // global, router level middleware
  // above are global
  // here is router level
  app.use('/inner', (req, res, next) => {
    next();
  });

  // middleware sub-stack
  // middleware vs. router
  // app.use(), app.get()
  // next('route')
  // next('router')

  /**
   * request -> (middleware) -> [route] -> {router...}
   */

  // error handler middleware, must the same signature
  // app.use((err, req, res, next) => {});

  // built in middleware & 3rd party
}

function learnOverriding() {
  // global override
  // @ts-ignore
  express.request.globalName = 'global name';

  // app override
  // @ts-ignore
  app.request.appName = 'app name';

  app.get('/', (req, res, next) => {
    // @ts-ignore
    console.log(req.globalName, req.appName);
    res.send('hello world');
  });

  // by prototype
  // Object.setPrototypeOf(Object.getPrototypeOf(app.request), FakeRequest.prototype)
}

function learnTemplate() {
  // we don't need it for now
}

function learnErrorHandling() {
  app.get('/', (req, res, next) => {
    // sync error handled by express by default
    // @ts-ignore
    res.send('hello world: ' + req.whoCare());
  });

  app.get('/async', async (req, res, next) => {
    // async error

    // this will cause crash, 5.0 will handle it
    // Promise.reject('bad!!!');

    // or next(err)
    // can set the status[Code], stack, headers
    next({ status: 444 });
  });

  // default error handler was put at last
  // NODE_ENV === 'product' to prevent error details

  // define error handler last
  // @ts-ignore
  app.use((err, req, res, next) => {
    // if written to response, must delegate to default handler
    if (res.headersSent) {
      return next(err);
    }

    res.status(500);
    res.send('big error ocurred!!!!');
  });
}

function learnDebug() {
  // $ DEBUG=express:* node index.js
  // DEBUG=express:* | application | router

  app.get('/', (req, res) => {
    res.send('hello world');
  });
}

function learnMiddlewareChain() {
  // will be from 1-1, from 2
  app.get(
    '/',
    (req, res, next) => {
      console.log('from 1-1');
      next('route');
    },
    (req, res, next) => {
      console.log('from 1-2');
      next();
    }
  );

  app.get('/', (req, res, next) => {
    console.log('from 2');
    next();
  });

  app.listen(9527, () => {
    console.log('server started on port 9527');
  });
}

function learnBehindProxy() {
  // when app behind a proxy
  app.set('trust proxy', 'bool' || 'ip' || 'number' || 'function');
}

function learnDBs() {
  // here is some db demo
  // https://expressjs.com/en/guide/database-integration.html
}

function learnProcessManager() {
  // should use a PM to run the app
  /**
   * PM2
   * Forever
   * Strong-PM
   * SystemD (linux built-in)
   */
}

function nativeNodeServer() {
  const http = require('http');
  const app = http.createServer((req, res) => {
    const { method, url, headers } = req;
    const body: any[] = [];

    // how to handle error (or app will crash) and get body
    req
      .on('error', (err: any) => {
        console.log(err);
      })
      .on('data', (chunk: any) => {
        body.push(chunk);
      })
      .on('end', () => {
        Buffer.concat(body).toString();
      });

    // the response
    res.statusCode = 404;
    // node send header for you, before body
    res.setHeader('x-header', 'value');
    // explicitly header
    res.writeHead(401, {
      'x-code': 'value',
    });

    // send back data, must after headers
    res.write('value');
    res.end('last one');

    // handle error for response
    res.on('error', () => {});

    // pipe is also ok
    req.pipe(res);
  });
  app.listen(8080);
}
