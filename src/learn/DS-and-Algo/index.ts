// @ts-nocheck

interface Node {
  value: any;
  next?: Node;
  prev?: Node;
}

class LinkedList {
  private list: Node | null = null;

  constructor(initNodes: any[] = []) {
    if (initNodes.length) {
      this.list = {
        value: initNodes[0],
      };

      let current = this.list;
      let node: Node;
      for (let i = 1; i < initNodes.length; i++) {
        node = {
          value: initNodes[i],
        };
        current.next = node;
        current = node;
      }
    }
  }

  prepend(value) {
    const node: Node = {
      value,
      next: this.list,
    };
    this.list = node;
  }

  append(value) {
    if (!this.list) {
      this.list = {
        value,
      };
      return;
    }

    let node = this.list;
    while (node.next) {
      node = node.next;
    }

    node.next = {
      value,
    };
  }

  toString() {
    if (!this.list) {
      return null;
    }

    let node = this.list;
    let desc = [];
    while (node) {
      desc.push(node.value);
      node = node.next;
    }

    return desc.join(' > ');
  }
}

const a = new LinkedList([1, 2, 3]);
a.prepend(5);
a.append(10);
console.log(a.toString());
