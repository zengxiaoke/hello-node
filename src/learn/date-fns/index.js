// @ts-nocheck

import { differenceInDays, format as dfFormat } from 'date-fns';
import { formatInTimeZone, utcToZonedTime, zonedTimeToUtc } from 'date-fns-tz';

function parseDate(date) {
  return new Date(date);
}

const timezones = {
  america_chicago: 'America/Chicago',
  asia_shanghai: 'Asia/Shanghai',
};

const formatTokens = {
  'e.g.-01:01 PM': 'h:mm a',
  'e.g.-Monday, January 01': 'eeee, MMMM dd',
  'e.g.-January 1st, 2000': 'MMMM do, yyyy',
  'e.g.-2000/01/01': 'yyyy/MM/dd',
  'e.g.-2000/01/01 01:01 PM': 'yyyy/MM/dd h:mm a',
};

function daysDiff(from, to) {
  return differenceInDays(parseDate(from), parseDate(to));
}

function format(date, formatString) {
  return dfFormat(parseDate(date), formatString);
}

function formatUTCToTimezone(utcDate, timezone, formatString) {
  return formatInTimeZone(parseDate(utcDate), timezone, formatString);
}

function formatBetweenTimezone(date, fromTimezone, toTimezone, formatString) {
  const utcDate = zonedTimeToUtc(parseDate(date), fromTimezone);
  return formatInTimeZone(utcDate, toTimezone, formatString);
}

export {
  timezones,
  formatTokens,
  format,
  daysDiff,
  formatUTCToTimezone,
  formatBetweenTimezone,
};
