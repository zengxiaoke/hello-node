const { Sequelize, DataTypes, Model, Op } = require('sequelize');
const seq = new Sequelize('sqlite::memory:', {
  // logging: false;
  // logging: console.log;

  define: {
    // some global options
    // freezeTableName: true
  },
});

(async () => {
  try {
    // test the connection
    await seq.authenticate();
    console.log('connected...');
  } catch (error) {
    console.log('error: ', error);
  }

  /**
   * here is you show
   */

  await queryBasics();

  seq.close();
})();

async function modelBasics() {
  const User = seq.define(
    'User',
    // attrs
    {
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: true, // by default
      },
      // short hand definition
      age: DataTypes.NUMBER,
      whenBirth: {
        type: DataTypes.DATE,
        // set the default value
        defaultValue: DataTypes.NOW,
      },
      uid: {
        type: DataTypes.UUID,
        // UUID default value
        defaultValue: DataTypes.UUIDV1, // or V4
      },
    },
    // options
    {
      // default to 'false', so the table name will be the plural form of model name
      freezeTableName: true,
      tableName: 'user_table',

      // no auto 'CreateAt', 'UpdateAt' attrs
      // timestamps: false
      // createdAt: '<your-name>',
      // updatedAt: false,

      indexes: [
        // create indexes
        {
          unique: true,
          fields: ['firstName', 'lastName'],
        },
      ],
    }
  );
  console.log('define return the model: ', User === seq.models.User);

  // used by define() internally
  class Dog extends Model {
    // not public field same name as attrs
    // firstName;
    // add your methods here...
  }
  Dog.init(
    // attrs
    {
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: true, // by default
      },
    },
    // options
    {
      sequelize: seq, // need the sequelize instance
      modelName: 'Dog',
    }
  );
  console.log('define return the model: ', Dog === seq.models.Dog);

  // sync

  // single sync
  // await User.sync({
  //   // delete then recreate
  //   force: true,
  //   // patch it
  //   alter: true,
  // });

  // sync all
  await seq.sync({
    force: true,
    // for safety check: only when db-name ends with _test
    // match: /_test$/
  });
  console.log('All models synced!!!');

  // dropping
  // await User.drop()
  await seq.drop();
  console.log('All tables dropped...');
}

async function modelInstance() {
  const User = seq.define('User', {
    name: DataTypes.STRING,
    age: DataTypes.INTEGER,
    birth: DataTypes.DATE,
  });
  await seq.sync({ force: true });
  console.log('done init db...');

  // add data
  const one = User.build({ name: 'one' }); // create only
  console.log('check rel: ', one instanceof User);
  // save it to db
  await one.save();
  // same as
  // await User.create({name: 'one'})

  // log the json, not the instance
  // console.log(one.toJSON());

  // update the db values
  one.name = 'other one';
  await one.save();
  // multi cols
  one.set({
    name: 'one',
    age: 12,
  });
  await one.save();
  // update()
  one.name = 'two';
  await one.update({ age: 20 });
  // name is still 'one'
  await one.save();
  // name is 'two' now

  // delete
  // await one.destroy()

  // reload
  one.name = 'xx';
  await one.reload();

  // save only some fields
  one.name = 'xx';
  one.age = 100;
  await one.save({ fields: ['name'] });
  await one.reload();
  console.log(one.age); // !== 100

  // save know what changed
  one.name = 'xx';
  await one.save(); // nothing to save

  // increment, decrement
  // inc by 1
  await one.increment('age');
  // inc by other
  await one.increment('age', { by: 2 });
  // object syntax
  await one.increment({
    age: 2,
  });
}

async function queryBasics() {
  const User = seq.define('User', {
    userName: {
      type: DataTypes.TEXT,
      validate: {
        len: [3, 6],
      },
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  });
  await seq.sync({ force: true });

  const one = await User.create(
    {
      userName: 'one',
      isAdmin: true,
    },
    {
      // only update these fields
      fields: ['userName'],
    }
  );
  const two = await User.create({
    userName: 'two',
    isAdmin: true,
  });

  // find all: select *
  let all = await User.findAll();
  all.forEach((item) => console.log(item.toJSON()));
  // not *
  all = await User.findAll({
    // rename
    // attributes: [['userName', 'user-name']],
    // aggregate
    // attributes: [
    //   'userName',
    //   'isAdmin',
    //   [seq.fn('count', seq.col('userName')), 'n_user'],
    // ],
    // short hand: include
    // attributes: {
    //   include: [
    //     // aggregate
    //     [seq.fn('count', seq.col('userName')), 'n_user'],
    //   ],
    // },
    // short hand: exclude
    // attributes: {
    //   exclude: ['userName'],
    // },
  });

  // operators
  all = await User.findAll({
    where: {
      // or diff cols
      [Op.or]: [{ userName: 'one' }, { isAdmin: false }],
      // or one col
      userName: {
        [Op.or]: ['one', 'three'],
      },
      // implicit in
      userName: ['one', 'two'],
    },
  });
  all.forEach((item) => console.log(item.toJSON()));

  // delete all
  // await User.destroy({
  //   truncate: true,
  // });

  // bulk create
  await User.bulkCreate(
    [{ userName: 'one' }, { userName: 'too long too fit' }],
    {
      // need validate? default to false
      // validate: true,
      // which fields to consider
      fields: ['userName'],
    }
  );

  // order & group
  await User.findAll({
    order: [['userName', 'ASC']],
    group: 'userName',
  });

  //  limit & offset
  await User.findAll({
    attributes: ['userName', 'isAdmin'],
    limit: 5,
    offset: 10,
  });

  // utilities methods
  await User.count();
  // min, max, sum
  await User.min('userName');
}
