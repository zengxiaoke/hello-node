const currentComponent = { value: null };
const renderedComponents = new Set();
const stateSetters = new Map();
const currentStates = new Map();

function useState(init) {
  let state, setState;
  const curFunc = currentComponent.value;

  if (renderedComponents.has(curFunc)) {
    // initiated: saved value and saved setter
    state = currentStates.get(curFunc);
    setState = stateSetters.get(curFunc);
  } else {
    // first time
    state = init;
    currentStates.set(curFunc, init);
    renderedComponents.add(curFunc);
    setState = (value) => {
      currentStates.set(curFunc, value);
    };
    stateSetters.set(curFunc, setState);
  }

  return [state, setState];
}

function render(component) {
  // set rendering 'func'
  currentComponent.value = component;
  component();
  // set end rendering 'func'
  currentComponent.value = null;
}

function one() {
  const [value, setValue] = useState(1);
  console.log('one value is: ', value);
  setValue(100);
}

function two() {
  const [value, setValue] = useState(2);
  console.log('two value is: ', value);
  setValue(200);
}

render(one); // >> one value is: 1
render(one); // >> one value is: 100

render(two); // >> two value is: 2
render(two); // >> two value is: 200
